﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.Aditional_Algorithms
{
    class Intersection_Polygon_With_Line : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            for (int i = 0; i < polygons[0].lines.Count; i++)
            {
                bool TurnTesting1 = false;
                bool TurnTesting2 = false;

                if (HelperMethods.CheckTurn(polygons[0].lines[i], lines[0].Start) != 
                    HelperMethods.CheckTurn(polygons[0].lines[i], lines[0].End))
                    TurnTesting1 = true;
                if (HelperMethods.CheckTurn(lines[0], polygons[0].lines[i].Start) != 
                    HelperMethods.CheckTurn(lines[0], polygons[0].lines[i].End))
                    TurnTesting2 = true;

                if (TurnTesting1 && TurnTesting2)
                {

                    IntersectionLine L = new IntersectionLine();
                    List<Line> Lines = new List<Line>();
                    Lines.Add(lines[0]);
                    Lines.Add(polygons[0].lines[i]);

                    List<Point> P1 = new List<Point>();

                    L.Run(null, Lines, null,ref P1, ref outLines , ref outPolygons);

                    //Point IntersectionPoint = new Point(XIntersection , YIntersection);
                    outPoints.Add(P1[0]);
                }

            }
        }

        public override string ToString()
        {
            return "Intersection Polygon With Line";
        }
    }
}
