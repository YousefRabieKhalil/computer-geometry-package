﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.Aditional_Algorithms
{
    class IntersectionLine : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            Check_intersection_Line(lines ,ref outPoints);
        }

        public override string ToString()
        {
            return "Line Intersection";
        }
        /// <summary>
        /// Caluclate Two Line Intersection
        /// </summary>
        /// <param name="lines"> The Two Line Lines[0] and Lines[1]</param>
        public void Check_intersection_Line(List<Line> lines , ref List<Point> outpoints)
        {
            // Find Slop Of First Line a1
            double A1 = (lines[0].End.Y - lines[0].Start.Y) / (lines[0].End.X - lines[0].Start.X);
            // substiute and Find b in in y = a1 x + b1 (Line One)
            double B1 = lines[0].Start.Y - (A1 * lines[0].Start.X);

            // Find Slop Of Second Line a2
            double A2 = (lines[1].End.Y - lines[1].Start.Y) / (lines[1].End.X - lines[1].Start.X);
            // substiute and Find b in in y = a1 x + b1 (Line One)
            double B2 = lines[1].Start.Y - (A2 * lines[1].Start.X);


            double IntersectionPointX = (B2 - B1) / (A1 - A2);
            double InterSectionPointY = A1 * IntersectionPointX + B1;

            outpoints.Add(new Point(IntersectionPointX , InterSectionPointY));
        }
    }
}
