﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremePoints : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
           
            outPoints = points;
            Random R = new Random();
            int Index = 0;
            if (outPoints.Count > 3)
            {
                for (Index = 0;Index<points.Count;Index++)
                {

                    bool found = false;
                    if (outPoints[Index] == null)
                        continue;

                    for (int i = 0; i < outPoints.Count; i++)
                    {
                        if (i == Index || outPoints[i] == null)
                            continue;

                        for (int j = 0; j < outPoints.Count; j++)
                        {
                            if (j == Index || outPoints[j] == null)
                                continue;

                            for (int k = 0; k < outPoints.Count; k++)
                            {
                                if (k == Index || outPoints[k] == null)
                                    continue;

                                Enums.PointInPolygon S = HelperMethods.PointInTriangle(outPoints[Index], outPoints[i], outPoints[j], outPoints[k]);
                                if (S == Enums.PointInPolygon.Inside || S == Enums.PointInPolygon.OnEdge)
                                {
                                    outPoints[Index] = null;
                                    found = true;
                                    break;
                                }
                            }

                            if (found)
                                break;
                        }
                        if (found)
                            break;
                    }
                    
                }
                List<Point> RealPoint = new List<Point>();
                foreach (var item in outPoints)
                {
                    if (item != null)
                        RealPoint.Add(item);
                }
                outPoints = RealPoint;
            }
            

        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Points";
        }
    }
}
