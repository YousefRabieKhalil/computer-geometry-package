﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class JarvisMarch : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {

           
            if (points.Count <= 2)
            {
                outPoints = points;
                return;
            }
            Point MinY = MinYPoint(points);
            Point Start = new Point(MinY.X, MinY.Y);
            outPoints = new List<Point>();
            Point ExtraPoint = new Point(MinY.X + 0.1, MinY.Y);
            Point TempPoint = new Point(double.MaxValue, double.MaxValue);
            outPoints.Add(MinY);
            while (true)
            {
                 double MaxAngle = double.MinValue;
                for (int i = 0; i < points.Count; i++)
                {
                    double angle =  HelperMethods.AngleBetweenPoints(MinY , ExtraPoint, points[i]);
                    if (angle == 180 && MaxAngle == double.MinValue)
                    {
                        MaxAngle = angle;
                        TempPoint = new Point(points[i].X, points[i].Y);
                    }
                    else if (angle == 180 || (angle == MaxAngle && angle != 0 && angle != -1))
                    {
                        if (angle == 180)
                            MaxAngle = 180;
                        if (points[i].X == TempPoint.X)
                        {
                            if (Math.Abs(points[i].Y - ExtraPoint.Y) > Math.Abs(TempPoint.Y - ExtraPoint.Y))
                            {
                                TempPoint = new Point(points[i].X, points[i].Y);
                            }
                            else
                            {
                                TempPoint = new Point(TempPoint.X, TempPoint.Y);
                            }
                        }
                        else if (points[i].Y == TempPoint.Y)
                        {
                            if (Math.Abs(points[i].X - ExtraPoint.X) > Math.Abs(TempPoint.X - ExtraPoint.X))
                            {
                                TempPoint = new Point(points[i].X, points[i].Y);
                            }
                            else
                            {
                                TempPoint = new Point(TempPoint.X, TempPoint.Y);
                            }
                        }
                        else
                        {
                            MaxAngle = angle;
                            TempPoint = new Point(points[i].X, points[i].Y);
                        }
                    }
                    else if (angle != -1 && angle > MaxAngle  && angle != 180 && angle!=0)
                    {
                        MaxAngle = angle;
                        TempPoint = new Point(points[i].X, points[i].Y);
                    }
                    
                }
                MinY = new Point(ExtraPoint.X, ExtraPoint.Y);
                ExtraPoint = new Point(TempPoint.X, TempPoint.Y);

                if (Start.Equals( ExtraPoint )|| MaxAngle == double.MinValue)
                {
                    break;
                }
                else
                    outPoints.Add(ExtraPoint);

            }

        }

        public override string ToString()
        {
            return "Convex Hull - Jarvis March";
        }
        public Point MinYPoint(List<Point> points)
        {
            Point Miny = new Point(double.MaxValue, double.MaxValue);
            foreach (Point my in points)
            {
                if (my.Y < Miny.Y)
                {
                    Miny = my;
                }
            }

            foreach (Point item in points)
            {
                if (Miny.Y == item.Y && item.X < Miny.X)
                {
                    Miny = new Point(item.X, item.Y);
                }
            }
            return Miny;
        }

    }
}
